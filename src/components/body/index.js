import { Component } from "react";
import { Note } from "./note";

class Body extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <Note text="Vou continuar praticando!" feeling={this.props.feeling} />
        </div>
      </div>
    );
  }
}
export default Body;
