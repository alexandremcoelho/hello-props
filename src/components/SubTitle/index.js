import { Component } from "react";
import { SubTitleNote } from "./SubTitleNote";
class Subtitle extends Component {
  render() {
    return (
      <h2>
        {this.props.text}
        <div>
          <SubTitleNote
            text="Manda mais repetição!"
            feeling={this.props.feeling}
          />
        </div>
      </h2>
    );
  }
}
export default Subtitle;
