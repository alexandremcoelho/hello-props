import { Component } from "react";

export class SubTitleNote extends Component {
  render() {
    return (
      <div>
        {this.props.text} {this.props.feeling}
      </div>
    );
  }
}
