import { Component } from "react";

export class FooterNote extends Component {
  render() {
    return (
      <div>
        {this.props.text} {this.props.feeling}
      </div>
    );
  }
}
