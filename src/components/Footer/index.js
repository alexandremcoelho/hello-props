import { Component } from "react";
import { FooterNote } from "./FooterNote";

class Footer extends Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <FooterNote text="Repetir mais!" feeling={this.props.feeling} />
        </div>
      </div>
    );
  }
}

export default Footer;
